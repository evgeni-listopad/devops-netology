# Домашнее задание к занятию "6.6. Troubleshooting"

## Задача 1

Перед выполнением задания ознакомьтесь с документацией по [администрированию MongoDB](https://docs.mongodb.com/manual/administration/).

Пользователь (разработчик) написал в канал поддержки, что у него уже 3 минуты происходит CRUD операция в MongoDB и её 
нужно прервать. 

Вы как инженер поддержки решили произвести данную операцию:
- напишите список операций, которые вы будете производить для остановки запроса пользователя
- предложите вариант решения проблемы с долгими (зависающими) запросами в MongoDB

### Решение задачи 1
```
1) Находим проблемную операцию с длительностью выполнения более 180 сек:
db.currentOp({ "active" : true, "secs_running" : { "$gt" : 180 }})
2) Из полученного вывода берем значение opid и подставляем в команду db.killOp:
db.killOp (занчение opid из запроса db.currentOp)
--------------------------------------------------------------------------------
Варианты решения проблемы с долгими (зависающими) запросами в MongoDB:
1) Можно на некоторое время включить и настроить Database Profiler,
чтобы фиксировать медленные операции для установления причин возникновения долгих запросов;
2) Можно использовать метод maxTimeMS() для установки предела времени в миллисекундах для обработки операций, 
однако в данном случае длительные операции будут принудительно прерываться механизмом db.killOp;
3) Можно воспользоваться командой explain для анализа длительных запросов.
И после выявления конкретной проблемы - возможно добавить/переделать индексацию либо по-другому выполнять запросы;
4) Попробовать внедрить горизонтальный шардинг для ограничения области обработки данных при выполнении запросов;
5) Выполнить мониторинг использования аппаратных ресурсов в периоды долгих (зависающих) запросов. 
В случае выявления острого недостатка конкретных аппаратных ресурсов - рассмотреть возможность аппаратной модернизации железа 
или виртуального добавления ресурсов (при работе в виртуальной среде).
```



## Задача 2

Перед выполнением задания познакомьтесь с документацией по [Redis latency troobleshooting](https://redis.io/topics/latency).

Вы запустили инстанс Redis для использования совместно с сервисом, который использует механизм TTL. 
Причем отношение количества записанных key-value значений к количеству истёкших значений есть величина постоянная и
увеличивается пропорционально количеству реплик сервиса. 

При масштабировании сервиса до N реплик вы увидели, что:
- сначала рост отношения записанных значений к истекшим
- Redis блокирует операции записи

Как вы думаете, в чем может быть проблема?
 
### Решение задачи 2
```
Согласно объяснению из вебинара и документации по Redis:
Большое количество ключей, срок действия которых истекает в один и тот же момент, может быть источником задержки.
"if the database has many, many keys expiring in the same second, 
and these make up at least 25% of the current population of keys with an expire set, 
Redis can block in order to get the percentage of keys already expired below 25%."
Как только количество "просроченных" ключей накапливается и превышает 25% от общего их количества, 
однопоточный Redis блокирует дальнейшую запись, чтобы не было перерасхода оперативной памяти.
Вариант выхода из ситуации - пытаться "разносить" ключи по времени,
тем самым избегая ситуации с одновременным истечением срока их действия.
```

## Задача 3

Вы подняли базу данных MySQL для использования в гис-системе. При росте количества записей, в таблицах базы,
пользователи начали жаловаться на ошибки вида:
```python
InterfaceError: (InterfaceError) 2013: Lost connection to MySQL server during query u'SELECT..... '
```

Как вы думаете, почему это начало происходить и как локализовать проблему?

Какие пути решения данной проблемы вы можете предложить?

### Решение задачи 3
```
Причины возникновения указанной ошибки могут быть следующие:
1) Возможно в SELECT-запросах пользователями запрашиваются слишком бальшие данные, 
размер которых превышает заданное в переменной max_allowed_packet значение.
2) Такая проблема также может возникать при нестабильной работе локальной сети и 
периодической потере доступа к MySQL-серверу.
3) К такой проблеме также может приводить "очень объемный" SELECT-запрос. То есть он 
не успевает передаться по сети за отведенное время, указанное в переменной net_read_timeout
(по умолчанию 30 сек.)
4) Такая проблема также может возникать при первоначальном подключении пользователя к MySQL-серверу
в условиях очень медленного сетевого соединения. То есть подключение по времени не укладывается в 
значение, заданное в переменной connect_timeout.
--------------------------------------------------------------------------------
Пути решения (в соответствии с вышеперечисленными причинами):
1) Увеличить значение, заданное в переменной max_allowed_packet.
2) Стабилизировать работу локальной сети, решив проблемы аппаратного уровня и настройки оборудования.
3) Увеличить значение, заданное в переменной net_read_timeout (например с 30 сек до 60 сек).
4) Увеличить значение, заданное в переменной connect_timeout или повысить скорость работы сети.
5) Если необходимо работать с "Большими данными", возможно начать использовать другую более подходящую для этого СУБД.
```

## Задача 4

Вы решили перевести гис-систему из задачи 3 на PostgreSQL, так как прочитали в документации, что эта СУБД работает с 
большим объемом данных лучше, чем MySQL.

После запуска пользователи начали жаловаться, что СУБД время от времени становится недоступной. В dmesg вы видите, что:

`postmaster invoked oom-killer`

Как вы думаете, что происходит?

Как бы вы решили данную проблему?

### Решение задачи 4
```
В системе наблюдается острая нехватка памяти.
Настолько острая, что запускается oom-killer (Out-Of-Memory Killer — процесс, 
который убивает другие процессы, чтобы спасти операционную систему от сбоя.)
Таким образом, проблема с СУБД PostgreSQL в первую очередь связана с нехваткой памяти, 
а во вторую - с вероятностью уничтожения процессов Postgres'а внутренней утилитой oom-killer.
--------------------------------------------------------------------------------
Пути решения проблемы:
1) Добавить оперативной памяти (аппаратно - при работе без виртуализации или виртуально - при работе с виртуальной машиной), 
а также организовать "быстрый" swap на высокоскоростных SSD. 
2) Ограничить потребление памяти СУБД PostgreSQL, оптимизировав значения max_connections, shared_buffer, work_mem,
effective_cache_size, maintenance_work_mem в конфигурационных файлах СУБД. 
Также будет полезно выполнить мониторинг использования памяти другими процессами на хосте.
Возможно где-то происходит утечка, которую нужно устранить.
3) Сделать процесс PostgreSQL менее "привлекательным" для oom-killer'а.
Например, в блоке [Service] системы инициализации systemd для PostgreSQL установить 
параметру OOMScoreAdjust большое отрицательное значение. 
Тогда oom-killer будет убивать другие процессы, а PostgreSQL останется работать.
```
Подробнее про настройку Out-Of-Memory Killer в Linux для PostgreSQL хорошо изложено в [Habr-статье](https://habr.com/ru/company/southbridge/blog/464245/).


