# Домашнее задание к занятию 10.5 «Платформа мониторинга Sentry»

## Задание 1

Так как Self-Hosted Sentry довольно требовательная к ресурсам система, мы будем использовать Free Сloud account.

Free Cloud account имеет ограничения:

- 5 000 errors;
- 10 000 transactions;
- 1 GB attachments.

Для подключения Free Cloud account:

- зайдите на sentry.io;
- нажмите «Try for free»;
- используйте авторизацию через ваш GitHub-аккаунт;
- далее следуйте инструкциям.

В качестве решения задания пришлите скриншот меню Projects.

## Задание 2

1. Создайте python-проект и нажмите `Generate sample event` для генерации тестового события.
1. Изучите информацию, представленную в событии.
1. Перейдите в список событий проекта, выберите созданное вами и нажмите `Resolved`.
1. В качестве решения задание предоставьте скриншот `Stack trace` из этого события и список событий проекта после нажатия `Resolved`.

## Задание 3

1. Перейдите в создание правил алёртинга.
2. Выберите проект и создайте дефолтное правило алёртинга без настройки полей.
3. Снова сгенерируйте событие `Generate sample event`.
Если всё было выполнено правильно — через некоторое время вам на почту, привязанную к GitHub-аккаунту, придёт оповещение о произошедшем событии.
4. Если сообщение не пришло — проверьте настройки аккаунта Sentry (например, привязанную почту), что у вас не было 
`sample issue` до того, как вы его сгенерировали, и то, что правило алёртинга выставлено по дефолту (во всех полях all).
Также проверьте проект, в котором вы создаёте событие — возможно алёрт привязан к другому.
5. В качестве решения задания пришлите скриншот тела сообщения из оповещения на почте.
6. Дополнительно поэкспериментируйте с правилами алёртинга. Выбирайте разные условия отправки и создавайте sample events. 

## Задание повышенной сложности

1. Создайте проект на ЯП Python или GO (около 10–20 строк), подключите к нему sentry SDK и отправьте несколько тестовых событий.
2. Поэкспериментируйте с различными передаваемыми параметрами, но помните об ограничениях Free учётной записи Cloud Sentry.
3. В качестве решения задания пришлите скриншот меню issues вашего проекта и пример кода подключения sentry sdk/отсылки событий.

---

### Решение заданий 1-3
* Скриншот меню Projects:
![Sentry1](./TASK_10.5/Sentry1.PNG)
* Сгенерированное тестовое событие:
![Sentry2](./TASK_10.5/Sentry2.PNG)
* Список событий проекта после нажатия `Resolved`:
![Sentry3](./TASK_10.5/Sentry3.PNG)
* Создано дефолтное правило алёртинга и сгенерировано событие `sample event`. Получено уведомление по почте:
![Sentry4](./TASK_10.5/Sentry4.PNG)

### Решение задания повышенной сложности
* В Pycharm выполнена установка `sentry-sdk`:
![Pycharm1](./TASK_10.5/Pycharm1.PNG)
* Создан примитивный проект на Python с подключенным sentry SDK. Сгенерировано несколько тестовых событий-ошибок:
![Pycharm2](./TASK_10.5/Pycharm2.PNG)
![Sentry5](./TASK_10.5/Sentry5.PNG)
* Зайдем в описание последнего события-ошибки в sentry.io и убедимся, что оно соответствует нашему Python-коду с ошибкой:
![Sentry6](./TASK_10.5/Sentry6.PNG)
* Также убедимся, что сработал алёртинг по возникшим событиям:
![Sentry7](./TASK_10.5/Sentry7.PNG)


